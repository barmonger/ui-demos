### What is this repository for? ###

I'll add UI demos as I try out things

### What is in the demo? ###

Currently only 3 login screen layouts:

# Box Login (login fields in a centered box/card)#
![Screenshot_20160227-221206.png](https://bitbucket.org/repo/n4M6XE/images/3863213042-Screenshot_20160227-221206.png)

# Wide Login (login fields in a wide highlighted ribbon)#
![Screenshot_20160227-221208.png](https://bitbucket.org/repo/n4M6XE/images/53898774-Screenshot_20160227-221208.png)

# Block Login (login fields in separate centered boxes)#
![Screenshot_20160227-221211.png](https://bitbucket.org/repo/n4M6XE/images/2139576931-Screenshot_20160227-221211.png)

### Who do I talk to? ###

Nicolaj Christensen (Barmonger)