package org.barmonger.uidemos.login;


import android.support.v4.app.Fragment;

public abstract class LoginFragment extends Fragment {

    @Override
    public void onResume() {
        super.onResume();
        ((LoginActivity) getActivity()).setupLoginViews(getView());
    }
}
