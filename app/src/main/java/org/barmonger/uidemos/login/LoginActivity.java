package org.barmonger.uidemos.login;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.barmonger.uidemos.R;

public class LoginActivity extends AppCompatActivity {

    private final Class[] fragments = {BoxedLoginFragment.class, BlockLoginFragment.class, WideLoginFragment.class};
    private int currentFragmentIndex = 0;
    private EditText usernameView;
    private EditText passwordView;
    String username;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupNextPreviousArrows();

        BoxedLoginFragment boxFragment = new BoxedLoginFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.login_fragment_container, boxFragment).commit();
    }

    private void setupNextPreviousArrows() {
        findViewById(R.id.previous_fragment_icon).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentFragmentIndex == 0) {
                    currentFragmentIndex = (fragments.length - 1);
                } else {
                    currentFragmentIndex--;
                }
                setFragment();
            }
        });
        findViewById(R.id.next_fragment_icon).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentFragmentIndex == (fragments.length - 1)) {
                    currentFragmentIndex = 0;
                } else {
                    currentFragmentIndex++;
                }
                setFragment();
            }
        });
    }

    private void setFragment() {
        try {
            username = usernameView.getText().toString();
            password = passwordView.getText().toString();
            Fragment fragment = (Fragment) fragments[currentFragmentIndex].newInstance();
            getSupportFragmentManager().beginTransaction().replace(R.id.login_fragment_container, fragment).commit();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("Could not instantiate fragment", e);
        }
    }

    void setupLoginViews(View currentFragment) {
        usernameView = (EditText) currentFragment.findViewById(R.id.username);
        passwordView = (EditText) currentFragment.findViewById(R.id.password);
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    doLogin();
                    return true;
                }
                return false;
            }
        });
        usernameView.setText(username);
        passwordView.setText(password);

        Button mSignInButton = (Button) currentFragment.findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();
            }
        });
    }

    public void doLogin() {
        String username = usernameView.getText().toString();
        String password = passwordView.getText().toString();

        if (!TextUtils.isEmpty(password) && !TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Logged in " + username, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Could not log in", Toast.LENGTH_SHORT).show();
        }
    }
}

