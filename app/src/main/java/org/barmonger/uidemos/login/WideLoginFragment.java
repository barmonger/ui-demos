package org.barmonger.uidemos.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.barmonger.uidemos.R;

/**
 * A login screen with wide login fields
 */
public class WideLoginFragment extends LoginFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wide_login, container, false);
    }
}

