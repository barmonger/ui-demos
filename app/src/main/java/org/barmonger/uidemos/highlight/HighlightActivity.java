package org.barmonger.uidemos.highlight;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import org.barmonger.uidemos.R;

public class HighlightActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private StaggeredGridLayoutManager layoutManager;
    private HighlightAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highlight);

        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.highlight_recycler);

        layoutManager = new StaggeredGridLayoutManager(2, GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new HighlightAdapter(getData());
        recyclerView.setAdapter(adapter);

        recyclerView.addItemDecoration(new SpacesItemDecoration(25));
    }

    private HighlightDataItem[] getData() {
        HighlightDataItem.HighlightType[] highlightTypes = HighlightDataItem.HighlightType.values();
        HighlightDataItem[] data = new HighlightDataItem[highlightTypes.length];
        for (int i = 0; i < highlightTypes.length; i++) {
            data[i] = createDataItem(highlightTypes[i]);
        }
        return data;
    }

    private HighlightDataItem createDataItem(HighlightDataItem.HighlightType highlightType) {
        return new HighlightDataItem(
                highlightType.name() + " header",
                highlightType.name() + " item1",
                highlightType.name() + " item2",
                highlightType);
    }
}
