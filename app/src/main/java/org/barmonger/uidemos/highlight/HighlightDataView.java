package org.barmonger.uidemos.highlight;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import org.barmonger.uidemos.R;

public class HighlightDataView extends CardView {

    public HighlightDataView(ViewGroup parent) {
        super(parent.getContext());
        LayoutInflater  mInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.item_highlight, this, true);
    }

    public void setDataItem(HighlightDataItem dataItem) {
        ((TextView) findViewById(R.id.cardHeader)).setText(dataItem.header);
        ((TextView) findViewById(R.id.cardItem1)).setText(dataItem.firstDataItem);
        ((TextView) findViewById(R.id.cardItem2)).setText(dataItem.secondDataItem);

        switch (dataItem.highlightType) {
            case NONE:
                break;
            case SIMPLE_ITEM:
                break;
            case SIMPLE_HEADER:
                break;
            case SIMPLE_CARD:
                break;
            case FULL_ITEM:
                break;
            case FULL_HEADER:
                break;
            case FULL_CARD:
                break;
        }
    }


}
