package org.barmonger.uidemos.highlight;

public class HighlightDataItem {
    public enum HighlightType {NONE, SIMPLE_ITEM, SIMPLE_HEADER, SIMPLE_CARD, FULL_ITEM, FULL_HEADER, FULL_CARD}

    public final String header;
    public final String firstDataItem;
    public final String secondDataItem;
    public final HighlightType highlightType;

    public HighlightDataItem(String header, String firstDataItem, String secondDataItem, HighlightType highlightType) {
        this.header = header;
        this.firstDataItem = firstDataItem;
        this.secondDataItem = secondDataItem;
        this.highlightType = highlightType;
    }
}
