package org.barmonger.uidemos.highlight;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public class HighlightAdapter extends RecyclerView.Adapter<HighlightAdapter.ViewHolder> {
    private HighlightDataItem[] highlightDataItems;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public HighlightDataView dataView;

        public ViewHolder(HighlightDataView dataView) {
            super(dataView);
            this.dataView = dataView;
        }
    }

    public HighlightAdapter(HighlightDataItem[] highlightDataItems) {
        this.highlightDataItems = highlightDataItems;
    }

    @Override
    public HighlightAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(new HighlightDataView(parent));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.dataView.setDataItem(highlightDataItems[position]);
    }

    @Override
    public int getItemCount() {
        return highlightDataItems.length;
    }
}